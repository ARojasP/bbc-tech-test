package alejandro.rojas.bbctechtest.model

import com.google.gson.annotations.SerializedName

data class ListFruitsResponse(
    @SerializedName("fruit")
    val fruits: List<Fruit>
)