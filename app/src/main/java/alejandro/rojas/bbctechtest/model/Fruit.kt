package alejandro.rojas.bbctechtest.model

import com.google.gson.annotations.SerializedName

data class Fruit(
    @SerializedName("type")
    val type: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("weight")
    val weight: String
)