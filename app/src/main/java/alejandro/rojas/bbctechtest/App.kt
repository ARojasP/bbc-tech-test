package alejandro.rojas.bbctechtest

import alejandro.rojas.bbctechtest.network.ApiClient
import alejandro.rojas.bbctechtest.ui.Constants
import android.app.Application
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        Thread.setDefaultUncaughtExceptionHandler { _, e ->
            ApiClient.getService()
                .recordEvent(Constants.EVENT_ERROR, e.message ?: "Unknown error")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

}