package alejandro.rojas.bbctechtest.ui.base

interface BaseView {

    fun showLoading()

    fun hideLoading()

    fun getInitialTime(): Long?

    fun getLastTime(): Long?
}