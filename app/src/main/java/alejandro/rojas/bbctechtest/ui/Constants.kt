package alejandro.rojas.bbctechtest.ui

object Constants{
    const val EXTRA_TYPE = "item_type_extra"
    const val EXTRA_PRICE = "item_price_extra"
    const val EXTRA_WEIGHT = "item_weight_extra"

    const val EVENT_LOAD = "load"
    const val EVENT_DISPLAY = "display"
    const val EVENT_ERROR = "error"
}