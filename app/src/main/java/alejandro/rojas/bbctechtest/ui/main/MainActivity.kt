package alejandro.rojas.bbctechtest.ui.main

import alejandro.rojas.bbctechtest.R
import alejandro.rojas.bbctechtest.model.Fruit
import alejandro.rojas.bbctechtest.ui.Constants
import alejandro.rojas.bbctechtest.ui.base.BaseActivity
import alejandro.rojas.bbctechtest.ui.detail.DetailActivity
import android.content.Intent
import android.os.Bundle
import android.widget.ListView

class MainActivity: BaseActivity<MainContract.View, MainPresenter>(), MainContract.View {

    override var presenter: MainPresenter = MainPresenter()
    lateinit var mainListView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainListView = findViewById(R.id.main_listview)

        presenter.loadFruits()
    }

    override fun showFruits(fruits: List<Fruit>) {

        val adapter = FruitAdapter(this, fruits)
        mainListView.adapter = adapter

        mainListView.setOnItemClickListener { _, _,position, _ ->
            val fruitSelected = fruits[position]

            val intent = Intent(this@MainActivity, DetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_TYPE, fruitSelected.type)
            intent.putExtra(Constants.EXTRA_PRICE, fruitSelected.price)
            intent.putExtra(Constants.EXTRA_WEIGHT, fruitSelected.weight)
            startActivity(intent)
        }
    }
}