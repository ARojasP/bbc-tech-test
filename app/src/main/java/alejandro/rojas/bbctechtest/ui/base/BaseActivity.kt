package alejandro.rojas.bbctechtest.ui.base

import alejandro.rojas.bbctechtest.R
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import java.util.*

abstract class BaseActivity<in V: BaseView, P: BasePresenter<V>>: AppCompatActivity(), BaseView {

    lateinit var spinner: ProgressBar
    protected abstract var presenter: P

    private var initialTime: Long? = null
    private var lastTime: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        initialTime = Date().time
        super.onCreate(savedInstanceState)
        presenter.attachView(this as V)
    }

    override fun onResume() {
        super.onResume()
        lastTime = Date().time
    }

    override fun onPostResume() {
        super.onPostResume()

        val time : Long? = (lastTime ?: 0) - (initialTime ?: 0)
        presenter.recordEventDisplay(time)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_base)
        val container: ViewGroup = findViewById(R.id.main_container)
        layoutInflater.inflate(layoutResID, container, true)

        spinner = findViewById(R.id.main_spinner)
    }

    override fun showLoading() {
        spinner.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        spinner.visibility = View.GONE
    }

    override fun getInitialTime():Long? {
        return initialTime
    }

    override fun getLastTime(): Long? {
        return lastTime
    }
}