package alejandro.rojas.bbctechtest.ui.main

import alejandro.rojas.bbctechtest.model.Fruit
import alejandro.rojas.bbctechtest.ui.base.BasePresenter
import alejandro.rojas.bbctechtest.ui.base.BaseView

object MainContract{

    interface View: BaseView {
        fun showFruits(fruits:List<Fruit>)
    }

    interface Presenter: BasePresenter<View> {
        fun loadFruits()
        fun recordEventLoad(time: Long)
    }

}