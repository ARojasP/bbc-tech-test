package alejandro.rojas.bbctechtest.ui.base

interface BasePresenter<in V: BaseView> {

    fun attachView(view: V)

    fun detachView()

    fun recordEventDisplay(time: Long?)
}