package alejandro.rojas.bbctechtest.ui.main

import alejandro.rojas.bbctechtest.R
import alejandro.rojas.bbctechtest.model.Fruit
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView


class FruitAdapter(var context: Context, var fruits: List<Fruit>): BaseAdapter() {

    internal class ViewHolder(view: View){
        var type: TextView = view.findViewById(R.id.fruit_listview_type)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.item_fruit, null)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        }else{
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var fruit = fruits[position]

        viewHolder.type.text = fruit.type
        return view
    }

    override fun getItem(position: Int): Fruit {
        return fruits[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return fruits.size
    }

}