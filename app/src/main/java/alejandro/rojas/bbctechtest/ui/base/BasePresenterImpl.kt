package alejandro.rojas.bbctechtest.ui.base

import alejandro.rojas.bbctechtest.network.ApiClient
import alejandro.rojas.bbctechtest.ui.Constants
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

abstract class BasePresenterImpl<V: BaseView>: BasePresenter<V> {

    protected var view: V? =null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }

    override fun recordEventDisplay(time: Long?) {
        ApiClient.getService()
            .recordEvent(Constants.EVENT_DISPLAY, time.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }
}