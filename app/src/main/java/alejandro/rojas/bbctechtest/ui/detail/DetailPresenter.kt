package alejandro.rojas.bbctechtest.ui.detail

import alejandro.rojas.bbctechtest.model.Fruit
import alejandro.rojas.bbctechtest.ui.Constants
import alejandro.rojas.bbctechtest.ui.base.BasePresenterImpl
import android.os.Bundle

class DetailPresenter: BasePresenterImpl<DetailContract.View>(), DetailContract.Presenter{

    override fun loadFruit(extras: Bundle?) {
        extras?.let {
            val fruit = Fruit(
                type = it.getString(Constants.EXTRA_TYPE) ?: "",
                price = it.getString(Constants.EXTRA_PRICE) ?: "",
                weight = it.getString(Constants.EXTRA_WEIGHT) ?: ""
            )

            view?.showFruit(fruit)
        }
    }

}