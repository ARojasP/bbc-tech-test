package alejandro.rojas.bbctechtest.ui.detail

import alejandro.rojas.bbctechtest.model.Fruit
import alejandro.rojas.bbctechtest.ui.base.BasePresenter
import alejandro.rojas.bbctechtest.ui.base.BaseView
import android.os.Bundle

object DetailContract {

    interface View: BaseView {
        fun showFruit(fruit: Fruit)
    }

    interface Presenter: BasePresenter<View> {
        fun loadFruit(extras: Bundle?)
    }
}