package alejandro.rojas.bbctechtest.ui.main

import alejandro.rojas.bbctechtest.network.ApiClient
import alejandro.rojas.bbctechtest.ui.Constants
import alejandro.rojas.bbctechtest.ui.base.BasePresenterImpl
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

class MainPresenter: BasePresenterImpl<MainContract.View>(), MainContract.Presenter {

    override fun loadFruits() {
        val initialTime = Date().time

        ApiClient.getService()
            .getFruits()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading() }
            .doOnTerminate { view?.hideLoading() }

            .subscribe { response ->
                val lastTime = Date().time
                recordEventLoad(initialTime - lastTime)
                view?.showFruits(response.fruits)
            }

    }

    override fun recordEventLoad(time: Long) {
        ApiClient.getService()
            .recordEvent(Constants.EVENT_LOAD, time.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

}