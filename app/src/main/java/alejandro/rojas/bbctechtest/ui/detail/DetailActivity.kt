package alejandro.rojas.bbctechtest.ui.detail

import alejandro.rojas.bbctechtest.R
import alejandro.rojas.bbctechtest.model.Fruit
import alejandro.rojas.bbctechtest.ui.base.BaseActivity
import android.os.Bundle
import android.widget.TextView

class DetailActivity: BaseActivity<DetailContract.View, DetailPresenter>(), DetailContract.View {

    override var presenter: DetailPresenter = DetailPresenter()

    private lateinit var type: TextView
    private lateinit var price: TextView
    private lateinit var weight: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        type = findViewById(R.id.detail_type)
        price = findViewById(R.id.detail_price)
        weight = findViewById(R.id.detail_weight)

        presenter.loadFruit(intent.extras)
    }

    override fun showFruit(fruit: Fruit) {
        type.text = fruit.type
        price.text = fruit.price
        weight.text = fruit.weight
        hideLoading()
    }
}