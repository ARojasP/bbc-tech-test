package alejandro.rojas.bbctechtest.network

import alejandro.rojas.bbctechtest.model.ListFruitsResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface ApiService {

    @GET("fmtvp/recruit-test-data/master/data.json")
    fun getFruits(): Observable<ListFruitsResponse>

    @GET("fmtvp/recruit-test-data/master/stats")
    fun recordEvent(@Query("event") event: String, @Query("data")  data: String): Observable<ResponseBody>

}